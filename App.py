# Imports
from flask import Flask, render_template, request, redirect, url_for, flash


app = Flask(__name__)

# Rutas/Routes


@app.route('/')
def Index():
    return render_template('index.html')


@app.route('/about')
def About():
    return render_template('about.html')


@app.route('/adopta')
def Adopta():
    return render_template('adopta.html')


@app.route('/edit')
def edit():
    return 'Editar'


@app.route('/delete')
def delete():
    return 'Eliminar'


@app.route('/contacto')
def Contacto():
    return render_template('contacto.html')


# Ejecutar servidor/Start server
if __name__ == '__main__':
    app.run(port=3000, debug=True)
