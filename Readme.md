# Refugio Garritas

## Contenidos

- [Información](#Información)
- [Tecnologías](#Tecnologías)
- [Objetivos](#Objetivos)

## Información

Éste proyecto es un ejemplo de creación de una página web en base a Python, Flask y base de datos.

## Tecnologías

Se han usado para su realización:

- Python 3 (Anaconda 3)
- Flask
- Bootstrap 4.3.1 min
- jquery 3.4.1

## Objetivos

- [x] Crear la estructura del servidor (App.py)
- [x] Crear la carpeta templates y probar la primera ruta (Index)
- [x] Comenzar con Bootstrap
- [ ] Mejorar Diseño
- [ ] Decidir que base de datos usar (SQLite, Postgresql)
